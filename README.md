# LEGO MOC Ferris Wheel

This Ferris Wheel is my first MOC submission.  I wanted to build something out of my technic which wasn't the typical wheel based vehicle type of construction - and a Ferris Wheel seemed like a suitable model 

It's made from Technic pieces from the sets I have, which are the big Technic sets from the 1990's.

It's just under 36cm high, 20cm wide and 26cm deep.

It's my first 'formal' MOC for the Lego community, so any constructive comments and/or suggestions are welcome

![alt text](https://gitlab.com/stuart-clarkson/lego-moc-39502-ferris-wheel/-/raw/73d032f4857eaeed7d0da7030643ddce6009f0b2/Images/2020-03%20Ferris%20Wheel.png "Ferris Wheel render")