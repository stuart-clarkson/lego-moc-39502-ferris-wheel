# Checklist

- [x] Create model
- [x] Create draft git repo (local repo only)
- [x] Create workable instructions
  - [x] Manually add lengths to technic axles
- [x] Re-build based on instructions
  - [x] Update instructions if required
- [x] Create draft MOC on rebrickable.com
- [x] Update git repo based on MOC number
- [x] Generate high-def renders
- [x] Finalise instructions
  - [x] Add bill of parts to (last page)
  - [x] Create front page
  - [x] Export as PDF, 2x size, save as ```MOC-<number> <title>.pdf```
- [x] Create __public__ Gitlab project titled `Lego MOC-<number> <title>`
- [x] Push to repo
- [x] Finalise MOC submission on rebrickable.com (use links to Gitlab files)